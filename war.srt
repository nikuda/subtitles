﻿1
00:01:38,200 --> 00:01:43,000
HOW THE WAR STARTED
ON MY ISLAND

2
00:02:54,800 --> 00:02:55,700
YEAR 1991.

3
00:02:56,000 --> 00:02:59,800
After the first free parliamentary
 elections, the Croatian Parliament

4
00:03:00,000 --> 00:03:03,800
made the decision to withdraw
 from the Yugoslav federation,

5
00:03:04,000 --> 00:03:07,800
and to establish an independent
and sovereign Republic of Croatia.

6
00:03:08,000 --> 00:03:11,800
On Croatian territory were barracks
 of the federal Yugoslav People's Army

7
00:03:12,000 --> 00:03:15,800
which only recognised the law
 of the Yugoslav federation,

8
00:03:16,000 --> 00:03:19,800
and ignored demands from
 citizens to leave Croatia,

9
00:03:20,000 --> 00:03:23,600
and to release the Croatian men
 who were in military service there.

10
00:03:29,660 --> 00:03:32,700
The Police representative
 stated that the reason

11
00:03:32,900 --> 00:03:35,600
the rifle grenade did not
 explode is unknown...

12
00:03:35,800 --> 00:03:39,100
...but it was likely due to
 unskilled handling during firing.

13
00:03:39,700 --> 00:03:43,400
In other crisis areas the tensions
did not ease during the night.

14
00:03:43,600 --> 00:03:46,100
Occasionally infantry
gunfire could be heard, but...

15
00:03:46,300 --> 00:03:49,500
...there were no reports of casualties,
 or of any material damage.

16
00:03:49,700 --> 00:03:53,200
According to the Police assessment
the gunfire was a provocation.

17
00:03:53,700 --> 00:03:58,200
However, driving on major roads 
 in crisis areas is at own risk.

18
00:03:59,000 --> 00:04:02,100
The decision on blockades of YPA
 barracks in Croatian cities...

19
00:04:02,300 --> 00:04:04,600
...is still being carried out
 in the Crisis HQ.

20
00:04:04,800 --> 00:04:08,100
Entries into the military facilities
are being blocked by citizens...

21
00:04:08,300 --> 00:04:09,900
...who are peacefully protesting.

22
00:04:30,210 --> 00:04:32,410
...forcefully held in parts
 of the barracks.

23
00:04:32,950 --> 00:04:35,600
A dozen or so young
 beardless men have fled

24
00:04:35,600 --> 00:04:37,980
from these barracks
 to freedom today.

25
00:04:37,980 --> 00:04:40,580
They are a testament
 that most of the men there

26
00:04:40,580 --> 00:04:43,180
do not want to fight
 against their neighbours.

27
00:04:43,180 --> 00:04:46,520
They also praised the improved
 conditions for the escapees.

28
00:04:46,520 --> 00:04:49,720
Newly established centres
 are providing them with shelter,

29
00:04:49,720 --> 00:04:53,170
clothes and advising them
on safe return to their homes.

30
00:05:29,900 --> 00:05:33,700
Excuse me, please. Do you know where
 is the barracks of National Heroes?

31
00:05:42,600 --> 00:05:46,170
Excuse me, do you know where
 is the barracks of National Heroes?

32
00:05:47,030 --> 00:05:48,030
No.

33
00:05:48,610 --> 00:05:50,610
There are no national heroes.

34
00:06:30,000 --> 00:06:31,700
Run!

35
00:06:37,900 --> 00:06:39,200
Delinquents.

36
00:06:39,300 --> 00:06:40,700
Excuse me...

37
00:06:40,800 --> 00:06:43,900
Every day they break one.
 – Can you help me?

38
00:06:44,000 --> 00:06:44,860
With what?

39
00:06:45,100 --> 00:06:46,560
I need to find the barracks.

40
00:06:46,820 --> 00:06:48,420
But you're not in the military?

41
00:06:48,771 --> 00:06:52,950
Military... I'm an art historian.

42
00:06:53,300 --> 00:06:55,703
I'm in a rush to get to...
 – Brilliant.

43
00:06:56,500 --> 00:06:59,600
You won't believe me, you know,
 I... paint some things.

44
00:06:59,700 --> 00:07:01,800
I throw my associations
onto the canvas.

45
00:07:01,900 --> 00:07:03,300
But there are no experts here.

46
00:07:03,400 --> 00:07:06,000
Listen, if you have time
 come with me to my atelier.

47
00:07:06,100 --> 00:07:07,300
You can see some of my work.

48
00:07:07,400 --> 00:07:08,400
I'm in a hurry, really.

49
00:07:08,500 --> 00:07:12,200
My son is in the military service here,
 I want to get him out somehow.

50
00:07:15,500 --> 00:07:17,300
Comrade major.
Private Sven Gajski.

51
00:07:17,500 --> 00:07:20,500
As you've ordered,
 I've collected all the transistors.

52
00:07:20,700 --> 00:07:23,600
The soldiers wish you
 good reception with pleasant music.

53
00:07:23,700 --> 00:07:25,200
Get out you imbecile.

54
00:07:25,400 --> 00:07:26,800
Understood.

55
00:07:43,240 --> 00:07:45,240
SERBIAN ARMY
OUT FROM CROATIA

56
00:07:51,860 --> 00:07:53,860
YU ARMY = ENEMIES

57
00:08:00,880 --> 00:08:02,880
Where are they...

58
00:08:10,960 --> 00:08:12,960
And, swallow my pain.

59
00:08:21,030 --> 00:08:22,800
Her shame is my shame.

60
00:08:26,790 --> 00:08:28,390
Where are you going?

61
00:08:28,390 --> 00:08:29,990
Down... to the fort.

62
00:08:29,990 --> 00:08:32,830
Are you normal?
It's better you stay here.

63
00:08:34,240 --> 00:08:36,640
Go ahead, but don't
 come crying back to me.

64
00:09:05,680 --> 00:09:07,080
Cannot comrade. Beat it.

65
00:09:07,880 --> 00:09:09,910
Can I speak to one of your higher ups?

66
00:09:09,910 --> 00:09:12,910
Cannot I'm saying to you.
  I cannot allow anyone inside.

67
00:09:13,480 --> 00:09:15,340
Can you call the officer on duty?

68
00:09:15,340 --> 00:09:18,090
I cannot, how can I move from here?

69
00:09:18,090 --> 00:09:20,680
C'mon, run before someone
comes who'll shoot. Go, go.

70
00:09:20,680 --> 00:09:24,660
Listen son, I'm the father of one
of your colleagues, Sven Gajski.

71
00:09:24,660 --> 00:09:25,960
I want to see him.

72
00:09:25,960 --> 00:09:28,080
Cannot. Negotiations are now.

73
00:09:28,080 --> 00:09:29,640
Just for a moment.

74
00:09:29,640 --> 00:09:32,320
Are you deaff? Here is
 not allowed to come close.

75
00:09:32,320 --> 00:09:34,850
If the major comes out
 then you, and we, and I,

76
00:09:34,850 --> 00:09:37,050
together with me are
 fucked. Go on, get out.

77
00:09:52,950 --> 00:09:54,450
I told you to stay here.

78
00:09:54,930 --> 00:09:55,930
What...

79
00:09:55,930 --> 00:09:57,280
Why are you sour?

80
00:09:57,280 --> 00:09:59,480
It'll be alright.
Roko will fix everything.

81
00:10:00,390 --> 00:10:01,190
Who is Roko?

82
00:10:01,190 --> 00:10:03,400
Roko Papak.
President of the Crisis.

83
00:10:03,400 --> 00:10:05,510
Him and Murko are at
 the negotiations inside,

84
00:10:05,510 --> 00:10:07,340
draining that crazy
 major of theirs.

85
00:10:16,040 --> 00:10:17,440
Schit down!

86
00:10:18,790 --> 00:10:20,840
I'm busting to piss, man.
I have to go.

87
00:10:20,840 --> 00:10:22,840
No ones moves until the major comes.

88
00:10:37,000 --> 00:10:37,660
Gajski?

89
00:10:37,660 --> 00:10:38,980
Yes, comrade major?

90
00:10:38,980 --> 00:10:41,510
How are you holding
that rifle? Firmer!

91
00:10:51,070 --> 00:10:53,080
Aleksa, listen.

92
00:10:53,080 --> 00:10:55,480
What's the matter Roko?
What do you want again?

93
00:10:56,510 --> 00:10:58,210
To persuade me to commit treason?

94
00:10:58,210 --> 00:11:00,610
Treason?
 Who mentioned treason?

95
00:11:01,550 --> 00:11:02,610
Aleksa.

96
00:11:03,080 --> 00:11:05,090
Comrade major to you.

97
00:11:05,090 --> 00:11:06,590
Forget about "comrade" for now.

98
00:11:06,590 --> 00:11:09,990
You should hand over the barracks,
  before the afternoon if possible.

99
00:11:10,000 --> 00:11:12,500
So these soldiers can get
  to the seven o'clock ferry.

100
00:11:13,010 --> 00:11:14,500
The civilian clothes are here.

101
00:11:14,500 --> 00:11:17,690
And the tickets for the
  ferry and bus are also here.

102
00:11:17,690 --> 00:11:19,560
Then we can go celebrate...

103
00:11:19,560 --> 00:11:20,860
First.

104
00:11:20,860 --> 00:11:23,360
Attach main line to the clamp.

105
00:11:24,310 --> 00:11:25,610
Second.

106
00:11:25,610 --> 00:11:28,960
Place inductor arm into the slot.

107
00:11:30,970 --> 00:11:32,070
Third.

108
00:11:32,070 --> 00:11:38,470
Rotate the inductor arm,
 two to three rotations per second.

109
00:11:38,470 --> 00:11:40,800
For ten seconds.

110
00:11:42,580 --> 00:11:43,880
Fourth.

111
00:11:44,770 --> 00:11:47,370
Insert the key in the keyhole.

112
00:11:48,800 --> 00:11:54,400
Finish the procedure by turning
  the key in to position: "Ignition".

113
00:12:03,610 --> 00:12:05,010
Listen, Roko.

114
00:12:06,780 --> 00:12:08,780
This here is a detonator.

115
00:12:10,160 --> 00:12:13,130
If anyone tries anything 
  against these barracks,

116
00:12:13,130 --> 00:12:15,850
I'll blow up these
 storehouses to the sky.

117
00:12:16,380 --> 00:12:17,780
It's all going to fly.

118
00:12:17,780 --> 00:12:20,980
Both you, and us. And the other
 half of the town will be poisoned.

119
00:12:20,980 --> 00:12:24,330
Go ahead now and attack
as much as you want.

120
00:12:28,260 --> 00:12:29,460
Are you crazy, Aleksa?

121
00:12:29,980 --> 00:12:33,290
Comrade major of
Yugoslav People's Army.

122
00:12:37,530 --> 00:12:40,960
Listen, we know each other
  for close to 10 years.

123
00:12:41,250 --> 00:12:42,850
You're like a local here.

124
00:12:42,850 --> 00:12:46,450
You live here, your friends
  are here... all those bodybuilders.

125
00:12:46,450 --> 00:12:47,830
And your wife is from here.

126
00:12:47,830 --> 00:12:49,020
No kidding?

127
00:12:49,020 --> 00:12:51,830
That's meant to be the reason 
  for me to change sides?

128
00:12:52,600 --> 00:12:54,100
So you can be my commander?

129
00:12:54,100 --> 00:12:56,500
Forget about "commander".
  Who knows who'll command who.

130
00:12:56,530 --> 00:12:59,430
Ten days ago you were
  changing taps on sinks.

131
00:12:59,990 --> 00:13:01,990
And now you want
  to command around?

132
00:13:02,620 --> 00:13:06,330
You... a plumber.
  To me, a professional.

133
00:13:07,070 --> 00:13:08,470
Private!

134
00:13:11,360 --> 00:13:12,760
Connect this.

135
00:13:22,340 --> 00:13:24,740
You really want to 
  blow everything up?

136
00:13:28,140 --> 00:13:31,260
I said what I needed to say.

137
00:13:31,260 --> 00:13:33,640
I really need to go for a piss now.

138
00:13:33,640 --> 00:13:35,200
No, you can't.

139
00:13:35,200 --> 00:13:38,500
In these barracks
  you can't even piss.

140
00:13:40,170 --> 00:13:41,970
Here they are. 
  Get up.

141
00:14:00,980 --> 00:14:04,190
Ok, enough.
Enough, cut it!

142
00:14:04,190 --> 00:14:05,540
Cut it off up there.

143
00:14:05,540 --> 00:14:07,540
Ok, ok, quiet down.

144
00:14:13,690 --> 00:14:14,990
Soldiers.

145
00:14:14,990 --> 00:14:18,790
Do not let the occupying
  YPA lead you to crime.

146
00:14:18,790 --> 00:14:22,390
We are not against you.
  We just want freedom.

147
00:14:22,390 --> 00:14:24,790
Croatia doesn't need 
  foreign barracks.

148
00:14:25,370 --> 00:14:26,870
Officers.

149
00:14:26,870 --> 00:14:32,200
Let the soldiers come out
 and embrace their peers.

150
00:14:32,480 --> 00:14:34,950
They were meant to be 
  here 3 hours ago.

151
00:14:35,630 --> 00:14:37,980
Have they left or not?

152
00:14:39,100 --> 00:14:42,980
I can't be in all places at once...
  Alright, what are you doing exactly?

153
00:14:42,980 --> 00:14:44,780
Mr. Papak.

154
00:14:46,280 --> 00:14:47,320
Here!

155
00:14:47,320 --> 00:14:49,570
Thank you God.
  But where were you?

156
00:14:49,570 --> 00:14:52,320
I was getting worried.
  Which group are you from?

157
00:14:52,320 --> 00:14:56,710
My name is Blaž Gajski.
– Guitar festival? '55, '56?

158
00:14:56,710 --> 00:15:00,130
It's all good, main thing 
  is that you know...

159
00:15:02,380 --> 00:15:05,630
My son is in the barracks here.
  I have to find a way to get him out.

160
00:15:06,620 --> 00:15:08,620
Are you trying to say
 you're not the drummer?

161
00:15:08,620 --> 00:15:11,650
Where are they? The show is dying,
 and this guy is no drummer.

162
00:15:13,080 --> 00:15:14,830
Well, did you hear 
  what I said? I need...

163
00:15:14,830 --> 00:15:16,990
I heard, I heard.
  Why are you yelling like that?

164
00:15:16,990 --> 00:15:18,760
Sorry, I'm starting 
  to lose patience.

165
00:15:18,760 --> 00:15:22,290
You think I'm not? Patience
is most important here.

166
00:15:22,290 --> 00:15:24,810
We have to wait...
 watch the show.

167
00:15:24,810 --> 00:15:28,010
It's of very high quality
  and on 24 hours a day.

168
00:15:28,010 --> 00:15:31,310
Roko!
  We're running a little late.

169
00:15:31,310 --> 00:15:34,710
I'm from the Comets, 
  and my boy plays in the Doors.

170
00:15:35,420 --> 00:15:41,840
And now, Comets with Vilim Šoda,
  and his boy from the Doors.

171
00:15:51,660 --> 00:15:53,840
What do you think?
  Is it more dynamic?

172
00:15:54,500 --> 00:15:56,570
To drive me nuts. Let's go.

173
00:16:01,230 --> 00:16:06,000
Branko! We're going to the
  headquarters. Keep an eye out.

174
00:16:13,480 --> 00:16:15,480
How do you not have it?

175
00:16:15,480 --> 00:16:18,760
You don't any phone
numbers for the barracks?

176
00:16:19,480 --> 00:16:21,980
Where can I find out?

177
00:16:23,010 --> 00:16:25,610
Excuse me, never mind.
  Goodbye.

178
00:16:45,000 --> 00:16:51,000
"CRISIS HEADQUARTERS"

179
00:17:06,720 --> 00:17:08,720
What?

180
00:17:08,720 --> 00:17:10,520
I need to see Mr. Papak.

181
00:17:10,520 --> 00:17:13,120
He's in there.
– Thanks.

182
00:17:22,590 --> 00:17:24,440
Close the fucking door.

183
00:17:29,510 --> 00:17:30,910
Mr. Papak.

184
00:17:32,580 --> 00:17:35,440
Only you have a telephone
connection to the barracks.

185
00:17:35,440 --> 00:17:40,120
  Now it's very important to call 
  somebody and explain the situation.

186
00:17:40,120 --> 00:17:41,640
My son...
– Who am I going to call?

187
00:17:41,640 --> 00:17:46,090
That crazy Aleksa? He won't
  talk to anyone but his command.

188
00:17:46,230 --> 00:17:48,890
Babe, don't. You know very 
  well that I'm surrounded.

189
00:17:50,190 --> 00:17:52,530
No, I haven't even been home.

190
00:17:53,340 --> 00:17:54,340
Go on, say?

191
00:17:54,630 --> 00:17:57,060
He shouldn't even be allowed
  to talk with his command.

192
00:17:59,540 --> 00:18:01,540
Yes, of course.

193
00:18:10,130 --> 00:18:12,540
Switchboard?
  Jozo? It's you.

194
00:18:12,540 --> 00:18:15,210
Listen, you have the
  barracks under you?

195
00:18:16,570 --> 00:18:19,020
Yeah, it's me,
 from the headquarters.

196
00:18:19,530 --> 00:18:22,530
Can you cut Aleksa's telephone?

197
00:18:24,610 --> 00:18:27,410
Nicely cut everything off now...

198
00:18:28,220 --> 00:18:29,270
Hello?

199
00:18:29,270 --> 00:18:31,270
Hello?!

200
00:18:31,270 --> 00:18:33,950
Hello – not my line... his line!

201
00:18:34,620 --> 00:18:36,220
Let's go back there.

202
00:18:37,480 --> 00:18:39,480
That's a smart uncle you have.

